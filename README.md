# Fake Campigotto
Fake Campigotto è un programma in Javascript che permette di gestire localmente
i punteggi di gare a squadre di matematica, o di simili competizioni. Il programma
è totalmente funzionante e segue quasi ogni regola ufficiale, ma non è ancora
né completo né documentato. Essendo questo un programma in Javascript è possibile
scaricare la pagina HTML per poi aprirla offline.

## Apology
A great chunk of this file was put together in an afternoon with no respect
for the tidiness of the code. I might try to update this program in the future
(some things are still missing), but since it aready fulfills its scope, it'll
never become much more complex or clean than it already is (on the bright side,
it will remain a lightweight and offline program).